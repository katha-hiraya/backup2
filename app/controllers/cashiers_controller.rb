class CashiersController < ApplicationController
  before_action :set_cashier, only: [:show, :edit, :update]
  
  def index
    @cashiers = Cashier.all
  end
  
  def new
    @cashier = Cashier.new
  end
  
  def create
    @cashier = Cashier.new(name: cashier_params["name"], status: cashier_params["status"], admin_id: current_admin.id, password: "password", email: cashier_params["email"])
    # raise @cashier.inspect
    if @cashier.save
      redirect_to @cashier
    end
  end
  
  def show
    
  end
  
  def edit
  end
  
  def update
    if @cashier.update(cashier_params)
      redirect_to @cashier
    end
  end
  
  def destroy
 # @cashier.status=false   
  # @cashier = @cashier.update(status: cashier_params["false"])
   if @cashier= Cashier.update(params[:id], status: false) 
      redirect_to cashier_path
    end
  end   
  

  
  private 
    def set_cashier
      @cashier = Cashier.find(params[:id])
    end
    def cashier_params
      params.require(:cashier).permit!
    end
  
end
