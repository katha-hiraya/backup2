class Admin < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable :registerable,   and :omniauthable
  devise :database_authenticatable, :recoverable, :registerable,
          :rememberable, :trackable, :validatable
         
  has_many :products
  has_many :cashiers
  
  
  
end
