class ProductsController < ApplicationController
  before_action :set_product, only: [:show, :edit, :update, :destroy]
  
  def index
    @products = Product.all
  end
  
  def new
    @product = Product.new
  end
  
  def create
    #product = Product.find(params[:product])
    @product = Product.new(product_params)
    if @product.save
        redirect_to @product
    else
      render :new
    end
  end

  def show
    
  end
  
  def update
    if @product.update(product_params)
      redirect_to @product
    end
  end
  
  def edit
  end
  
  def destroy
    if @product = Product.update(params[:id], product_status: false)
      redirect_to products_path
    end
  end
  
  private
    def set_product
      @product = Product.find(params[:id])
    end
    def product_params
      params.require(:product).permit!
    end
end
