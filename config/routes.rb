Rails.application.routes.draw do
  
    
    devise_for :admins
 
    devise_scope :admin do
      authenticated :admin do
        root 'admins#index', as: :authenticated_root
    
        get 'admin/index'
            resources :admins
            
        get 'products/create_products' #if not working ~> change to admings
           resources :products
           
        get 'cashiers/create'
          resources :cashiers
        
        end
      end
     
    
    unauthenticated do 
	    root 'devise/sessions#new', as: :unauthenticated_root
	
    end
      
  
 devise_for :cashiers
  #devise_scope: :cashier do #trial
    #authenticated :cashier do
      #root 'cashiers'
    #end
    #end
end
  #get 'cashiers/create'
      #resources :cashiers

  #get 'admins/index'
      #resources :admins
      
 # get 'products/create_products'
  #    resources :products
   

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

