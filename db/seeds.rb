# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# Admin.create!([{email: 'asvus@admin.com', password: 'asvus', password_confirmation: 'asvus'}])
# Product.create!([{product_name: 'adsad', product_price: '1213'}])

=begin
@admin = Admin.new
@admin.email = 'asvus@admin.com'
@admin.encrypted_password = 'asvus'
@admin.save!
=end

admin = Admin.find_or_initialize_by(email: 'asvus@admin.com')
admin.password = 'administrator'
admin.save!

=begin
sources: https://stackoverflow.com/questions/18569240/how-to-add-new-seed-data-to-existing-rails-database
         https://stackoverflow.com/questions/4116067/purge-or-recreate-a-ruby-on-rails-database
=end
